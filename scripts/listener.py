#!/usr/bin/env python
import rospy
from std_msgs.msg import Bool

Button1 = False
Button2 = False
Button3 = False

def callback1(data):
    global Button1 
    Button1 = data.data
    rospy.loginfo(rospy.get_caller_id() + " Button1 state %s", data.data)
    

def callback2(data):
    global Button2 
    Button2 = data.data
    rospy.loginfo(rospy.get_caller_id() + " Button2 state %s", data.data)
    

def callback3(data):
    global Button3 
    Button3 = data.data   
    rospy.loginfo(rospy.get_caller_id() + " Button3 state %s", data.data) 
    

def listener():

    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber("pushed1", Bool, callback1)
    rospy.Subscriber("pushed2", Bool, callback2)
    rospy.Subscriber("pushed3", Bool, callback3)
    
    pub = rospy.Publisher('stop_light', Bool, queue_size=10)

    rate = rospy.Rate(4)
    while not rospy.is_shutdown():
        if Button1==True and Button2==True and Button3==True:
            stoplightbool = False
        else:
            stoplightbool = True

        rospy.loginfo(stoplightbool)
        pub.publish(stoplightbool)
        rospy.sleep(10)

    rospy.spin()

    

if __name__ == '__main__':
    try:
        listener()
    except rospy.ROSInterruptException:
        pass